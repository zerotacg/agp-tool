import java.io.File
import java.nio.ByteBuffer
import java.nio.ByteOrder.LITTLE_ENDIAN

data class Axis(
    val x: Short,
    val y: Short,
)

data class Frame(
    val leftStick: Axis,
    val rightStick: Axis,
    val unkown_0: Byte,
    val leftTriggerAxis: Byte,
    val unkown_1: Byte,
    val rightTriggerAxis: Byte,
    val buttonBitMask: Short,
    val dpad: Byte,
    val unkown_2: Byte,
)

val EMPTY = ByteArray(0x10).apply { fill(0xff.toByte()) };

@OptIn(ExperimentalStdlibApi::class)
fun main(args: Array<String>) {
    println("Program arguments: ${args.joinToString()}")
    val filename = args.firstOrNull()
    if (filename == null) {
        println("no file given")
        return
    }

    val bytes = ByteArray(0x10)
    File(filename).inputStream().use {
        while (true) {
            val sz = it.read(bytes)
            if (sz <= 0) break

            val buffer = ByteBuffer.wrap(bytes).order(LITTLE_ENDIAN)
            println(bytes.toHexString())
            if (bytes.contentEquals(EMPTY)) break
            val someStruct = Frame(
                Axis(buffer.getShort(), buffer.getShort()),
                Axis(buffer.getShort(), buffer.getShort()),
                buffer.get(),
                buffer.get(),
                buffer.get(),
                buffer.get(),
                buffer.getShort(),
                buffer.get(),
                buffer.get(),
            )

            println(someStruct)
        }
    }
}
